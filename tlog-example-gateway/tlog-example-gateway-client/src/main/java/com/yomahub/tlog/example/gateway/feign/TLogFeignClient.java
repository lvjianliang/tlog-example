package com.yomahub.tlog.example.gateway.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("tlog-gateway")
public interface TLogFeignClient {
    @RequestMapping(value = "/demo/hi",method = RequestMethod.GET)
    public String sayHello(@RequestParam(value = "name") String name);
}
