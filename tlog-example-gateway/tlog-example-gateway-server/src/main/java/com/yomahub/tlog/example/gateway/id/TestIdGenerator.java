package com.yomahub.tlog.example.gateway.id;

import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.yomahub.tlog.constant.TLogConstants;
import com.yomahub.tlog.id.TLogIdGenerator;
import org.springframework.web.server.ServerWebExchange;

public class TestIdGenerator extends TLogIdGenerator {

    @Override
    public String generateTraceId() {
        ServerWebExchange exchange = (ServerWebExchange)this.extData.get(TLogConstants.WEBFLUX_EXCHANGE);
        if (ObjectUtil.isNotNull(exchange) && ObjectUtil.isNotNull(exchange.getRequest().getQueryParams().getFirst("prefix"))){
            String id = StrUtil.format("{}_{}",exchange.getRequest().getQueryParams().getFirst("prefix"), UUID.fastUUID().toString());
            return id;
        }else{
            return UUID.fastUUID().toString();
        }
    }
}
