package com.example.customlabel.vo;

public class Address {

    private String country;

    private String company;

    public Address() {
    }

    public Address(String country, String company) {
        this.country = country;
        this.company = company;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
