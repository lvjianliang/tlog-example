package com.example.customlabel;

import com.example.customlabel.vo.Address;
import com.example.customlabel.vo.Person;
import com.yomahub.tlog.core.annotation.TLogAspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SpringBootApplication
@EnableAsync
@RestController
public class RunnerApplication {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TestDomain testDomain;

    public static void main(String[] args) {
        SpringApplication.run(RunnerApplication.class, args);
    }

    //这里有个特例要注意下，如果调用同类方法下的@TLogAspect标签，不会生效
    //这是因为受到切面的限制，spring下同类下面调用，切面是不会生效的
    @RequestMapping("/hi")
    @TLogAspect("name")
    public String sayHello(@RequestParam String name){
        log.info("invoke method sayHello,name={}",name);
        Person person = initPerson();
        testDomain.testMethod(person);
        return "hello";
    }

    private Person initPerson(){
        Address address = new Address("中国", "上海牛逼哄哄科技有限公司");
        Person person = new Person(24L, "Bryan.Zhang", address);
        return person;
    }

}
