package com.yomahub.tlog.example.springnative.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @RequestMapping("/hi")
    public String sayHello(@RequestParam String name){
        log.info("invoke demo method sayHello");
        return "hello,"+name;
    }
}